# class Cat:
#
#     CATS_CREATED = 0
#
#     def __init__(self, name, color):
#         self._name = name
#         self._color = color
#         Cat.CATS_CREATED += 1
#
#     def say_meow(self):
#         print('MEOW!')
#
#     def walk_around(self):
#         print('The cat walks around')
#
#     def eat(self):
#         print('Cat eat')
#
#
#     def who_ami(self):
#         print(f'I am {self._name}')
#
#     def get_name(self):
#         return self._name
#
#     def set_name(self, name):
#         if name in ['Барсик', 'Пушок']:
#             raise Exseption('Not available name')
#         self._name = name
#
#
# cat = Cat('Alex', 'grey')
# cat.set_name('Felix')
# print(cat.get_name())
# cat2 = Cat('Geo', "black")
# print(Cat.CATS_CREATED)


# class GlobalVarExampleClass:
#     GLOBAL_VAR_VALUE = 1
#
#     def chec_cccess_to_class_var(self):
#         return self.GLOBAL_VAR_VALUE
#
#     def set_class_var_value(self, value):
#         GlobalVarExampleClass.GLOBAL_VAR_VALUE = value
#
#
# obj = GlobalVarExampleClass()
# print(obj.chec_cccess_to_class_var())
# obj.set_class_var_value(10)
# print(obj.chec_cccess_to_class_var())
# print(GlobalVarExampleClass.GLOBAL_VAR_VALUE)


# class Vehicle:
#     NUM_OF_DOORS = 4
#     FUEL_TYPE = 'Gas'
#
#     def move(self):
#         print('Car drives')
#
#     def add_fuel(self, value):
#         self._fuel +=value
#
#     def get_fuel(self):
#         return self._fuel
#
#     def get_brand(self):
#         return self._brand
#
#     def set_brand(self, value):
#         self._brand = value
#
#     def get_engine(self):
#         return self._engine
#
#     def set_engine(self, value):
#         self._engine = value
#
#
# class Car(Vehicle):
#
#     def __init__(self, brand, engine):
#         self._brand = brand
#         self._engine = engine
#         self._fuel = 0
#
#     def move(self):
#         print('Move about 100 km/h')
#
#     def __str__(self):
#         return f'Brand {self._brand}'
#
#
# car = Car('bmw', 'v8')
# print(car.get_engine())
# print(car.get_brand())
# car.move()
#
# print(car)
#
#
# class Example:
#
#     __slots__ = ("name")
#
#     def __init__ (self, name):
#         self._name = name
#
#
# obj = Example('example obj')
#
# obj._name = "new"
# #obj._my_cool_var = 11
